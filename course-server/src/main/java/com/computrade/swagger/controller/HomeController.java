package com.computrade.swagger.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UncheckedIOException;

import static java.nio.charset.StandardCharsets.UTF_8;

@RestController
public class HomeController {


    @Value("classpath:swagger.json")
    private Resource resource;

    @RequestMapping("/course/api/v1.0/spec")
    public String getSwaggerSpec() {

        String response;
        try (Reader reader = new InputStreamReader(resource.getInputStream(), UTF_8)) {
            response = FileCopyUtils.copyToString(reader);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }

        return  response;

    }

}
